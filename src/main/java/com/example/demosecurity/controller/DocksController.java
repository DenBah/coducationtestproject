package com.example.demosecurity.controller;

import com.example.demosecurity.model.Document;
import com.example.demosecurity.repository.DocumentRepository;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


@RestController
public class DocksController {
    private DocumentRepository documentRepository;

    public DocksController(DocumentRepository documentRepository) {
        this.documentRepository = documentRepository;
    }

    @PostMapping("/add")
    public ResponseEntity<Long> addDocument(@RequestParam("file") MultipartFile file) {
        if (!file.isEmpty()) {
            try {
                Document document = new Document();
                document.setName(file.getOriginalFilename());
                document.setContent(file.getBytes());

                Document savedDocument = documentRepository.save(document);
                return ResponseEntity.ok(savedDocument.getId());
            } catch (IOException e) {
                e.printStackTrace();
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

    @GetMapping("/download/{documentId}")
    public ResponseEntity<Resource> downloadDocument(@PathVariable Long documentId) {
        return documentRepository.findById(documentId)
                .map(document -> {
                    ByteArrayResource resource = new ByteArrayResource(document.getContent());
                    HttpHeaders headers = new HttpHeaders();
                    headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + document.getName() + "\"");
                    return ResponseEntity.ok()
                            .headers(headers)
                            .contentLength(document.getContent().length)
                            .contentType(MediaType.APPLICATION_OCTET_STREAM)
                            .body((Resource) resource);
                })
                .orElse(ResponseEntity.notFound().build());
    }
}