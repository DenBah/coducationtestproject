package com.example.demosecurity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.demosecurity.model.Document;

public interface DocumentRepository extends JpaRepository<Document, Long> {
}
